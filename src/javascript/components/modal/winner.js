import { createElement } from '../../helpers/domHelper';
import {showModal} from './modal';

export function showWinnerModal(fighter) {
  const title = 'The Winner is...';
  const bodyElement = createBodyElement(fighter);
  
  showModal({title, bodyElement, onClose: ()=>location.reload()});
}

function createBodyElement(fighter) {
  const { source, name } = fighter;
  const winnerElement = createElement({
    tagName: 'div',
    className: 'winner',
  });
  const winnerName = createElement({
    tagName: 'h2'
  });

  const attributes = { 
    src: source,
    title: name,
    alt: name, 
  };

  const winnerImg = createElement({
    tagName: 'img',
    className: 'winnerImage',
    attributes
  })
  winnerName.innerHTML = name;

  winnerElement.append(winnerName, winnerImg);
  return winnerElement;
}