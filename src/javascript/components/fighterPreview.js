import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if(typeof fighter != 'undefined'){
    const fighterImage = createFighterImage(fighter);
    const fighterName = createFighterName(fighter);
    const fighterIndicators = createFightersIndicators(fighter);
    console.log(fighterIndicators);
    fighterElement.append(fighterImage, fighterName, fighterIndicators);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterName(fighter){
  const { name } = fighter;
  const nameElement = createElement({
    tagName: 'h2',
    className: 'fighter-preview_name'
  });
  nameElement.innerText = name;

  return nameElement;
}

export function createFightersIndicators(fighter){
  //const { attack, defense, health } = fighter;

  const fightersIndicatorsContainer = createElement({
    tagName: 'ul',
    className: 'fighter-preview_indicators'
  })

  for(let indicator in fighter){
    if(indicator == 'attack' || indicator == 'defense' || indicator == 'health'){
      let indicatorElement = createFightersIndicator(indicator, fighter[indicator]);
      fightersIndicatorsContainer.append(indicatorElement);
    }
  }

  return fightersIndicatorsContainer
}


export function createFightersIndicator(name, value){
  //const { attack, defense, health } = fighter;

  const fightersIndicatorsContainer = createElement({
    tagName: 'li',
    className: 'fighter-preview_indicator'
  })

  fightersIndicatorsContainer.innerText = `${name} - ${value}`;

  return fightersIndicatorsContainer
}
