import { controls } from '../../constants/controls';
import {showWinnerModal} from './modal/winner';

export async function fight(firstFighter, seconFighter) {
  let keyPressed = [];
  let winner = undefined;
  let firstFighterHealth = firstFighter.health;
  let seconFighterHealth = seconFighter.health;
  let damage = 0;

  window.addEventListener('keydown', event => {
    if(keyPressed.indexOf(event.code)< 0){
      keyPressed.push(event.code);
    } 
    winner = checkWin();
    if(winner){
      return new Promise((resolve) => {
        resolve(showWinnerModal(winner));
      });
    }
  }) 

  window.addEventListener('keyup', event => {
    keyPressed = keyPressed.filter(key=> key != event.code);
  });

  function checkWin(){
    if(keyPressed.indexOf(controls.PlayerOneAttack)>=0 && keyPressed.indexOf(controls.PlayerTwoBlock)<0){
      damage = getDamage(firstFighter, seconFighter);
      seconFighterHealth = seconFighterHealth - damage;
      showHealth(seconFighter, seconFighterHealth, 'right');
    }
    else if(keyPressed.indexOf(controls.PlayerOneAttack)>=0 && keyPressed.indexOf(controls.PlayerTwoBlock)>0){
      damage = firstFighter.attack;
      seconFighterHealth = seconFighterHealth - damage;
      showHealth(seconFighter, seconFighterHealth, 'right');
    }
    else if(keyPressed.indexOf(controls.PlayerTwoAttack)>=0 && keyPressed.indexOf(controls.PlayerOneBlock)<0){
      damage = getDamage(seconFighter, firstFighter);
      firstFighterHealth = firstFighterHealth - damage;
      showHealth(firstFighter, firstFighterHealth, 'left');
    }
    else if(keyPressed.indexOf(controls.PlayerTwoAttack)>=0 && keyPressed.indexOf(controls.PlayerOneBlock)>0){
      damage = seconFighter.attack;
      firstFighterHealth = firstFighterHealth - damage;
      showHealth(firstFighter, firstFighterHealth, 'left');
    }
    else if(controls.PlayerOneCriticalHitCombination.every(keyCode => keyPressed.indexOf(keyCode)>=0)){
      damage = firstFighter.attack * 2;
      seconFighterHealth = seconFighterHealth - damage;
      showHealth(seconFighter, seconFighterHealth, 'right');
    }
    else if(controls.PlayerTwoCriticalHitCombination.every(keyCode => keyPressed.indexOf(keyCode)>=0)){
      damage = seconFighter.attack * 2;
      firstFighterHealth = firstFighterHealth - damage;
      showHealth(firstFighter, firstFighterHealth, 'left');
    }
    
    if(firstFighterHealth < 0){
      return seconFighter;
    }else if(seconFighterHealth < 0) {
      return firstFighter
    }
    else{
      return undefined;
    }
  }
}


export function getDamage(attacker, defender) {
  // return damage
  const hit = getHitPower(attacker);
  const block = getBlockPower(defender);
  const damage = (hit - block) > 0 ? (hit - block): 0
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() * 2 + 1;
  const attackPower = attack * criticalHitChance;
  return attackPower;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() * 2 + 1;
  const defencePower = defense * dodgeChance;
  return defencePower;
}


function showHealth(fighter, fighterNewHealth, indicator){
  const fighterHealthBar = document.querySelector(`#${indicator}-fighter-indicator`);
  const fighterHealthBarWidth = fighterNewHealth / fighter.health * 100;
  fighterHealthBar.style.width = `${fighterHealthBarWidth}%`;
}